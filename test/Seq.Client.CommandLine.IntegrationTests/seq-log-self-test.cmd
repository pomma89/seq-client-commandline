@echo off

REM Simple messages, one per log level.
call seq-log -s "self-test" -l verbose -m "Verbose test message from Seq command-line client"
call seq-log -s "self-test" -l debug -m "Debug test message from Seq command-line client"
call seq-log -s "self-test" -l information -m "Information test message from Seq command-line client"
call seq-log -s "self-test" -l warning -m "Warning test message from Seq command-line client"
call seq-log -s "self-test" -l error -m "Error test message from Seq command-line client"
call seq-log -s "self-test" -l fatal -m "Fatal test message from Seq command-line client"

REM Simple messages, one per log level, with short log level syntax.
call seq-log -s "self-test" -l v -m "Verbose test message from Seq command-line client - Short log level syntax"
call seq-log -s "self-test" -l d -m "Debug test message from Seq command-line client - Short log level syntax"
call seq-log -s "self-test" -l i -m "Information test message from Seq command-line client - Short log level syntax"
call seq-log -s "self-test" -l w -m "Warning test message from Seq command-line client - Short log level syntax"
call seq-log -s "self-test" -l e -m "Error test message from Seq command-line client - Short log level syntax"
call seq-log -s "self-test" -l f -m "Fatal test message from Seq command-line client - Short log level syntax"

REM Messages with properties, one per log level.
call seq-log -s "self-test" -l v -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:VERBOSE|TestNumber:1"
call seq-log -s "self-test" -l d -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:DEBUG|TestNumber:2"
call seq-log -s "self-test" -l i -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:INFORMATION|TestNumber:3"
call seq-log -s "self-test" -l w -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:WARNING|TestNumber:4"
call seq-log -s "self-test" -l e -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:ERROR|TestNumber:5"
call seq-log -s "self-test" -l f -m "{TestLogLevel} test template message from Seq command-line client - {TestNumber}" -p "TestLogLevel:FATAL|TestNumber:6"

REM Logging an error message as an exception.
call seq-log -s "self-test" -l v -m "Verbose test exception message from Seq command-line client" -x "Detailed error message"
call seq-log -s "self-test" -l d -m "Debug test exception message from Seq command-line client" -x "Detailed error message"
call seq-log -s "self-test" -l i -m "Information test exception message from Seq command-line client" -x "Detailed error message"
call seq-log -s "self-test" -l w -m "Warning test exception message from Seq command-line client" -x "Detailed error message"
call seq-log -s "self-test" -l e -m "Error test exception message from Seq command-line client" -x "Detailed error message"
call seq-log -s "self-test" -l f -m "Fatal test exception message from Seq command-line client" -x "Detailed error message"

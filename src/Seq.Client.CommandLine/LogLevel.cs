﻿namespace Seq.Client.CommandLine
{
    internal enum LogLevel
    {
        Verbose,
        Debug,
        Information,
        Warning,
        Error,
        Fatal
    }
}
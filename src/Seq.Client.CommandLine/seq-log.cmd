@echo off

set SEQ_BASE_URL=???
set SEQ_DEFAULT_API_KEY=???
set DEFAULT_PROPERTIES=HostName:%COMPUTERNAME%

Seq.Client.CommandLine.exe -b "%SEQ_BASE_URL%" -k "%SEQ_DEFAULT_API_KEY%" -d "%DEFAULT_PROPERTIES%" %*

﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Seq.Client.CommandLine
{
    internal sealed class LogEvent
    {
        public DateTimeOffset Timestamp { get; set; }

        public LogLevel LogLevel { get; set; }

        public string MessageTemplate { get; set; }

        public string SourceContext { get; set; }

        public string Properties { get; set; }

        public string DefaultProperties { get; set; }

        public string Exception { get; set; }

        public string ToClef() => ToJObject().ToString(Formatting.None);

        private JObject ToJObject()
        {
            var obj = new JObject
            {
                { "@t", Timestamp },
                { "@l", LogLevel.ToString() },
                { "@mt", MessageTemplate },
                { nameof(SourceContext), SourceContext }
            };

            if (!string.IsNullOrWhiteSpace(Exception))
            {
                obj["@x"] = Exception;
            }

            ParseProperties(obj, DefaultProperties);
            ParseProperties(obj, Properties);

            return obj;
        }

        private static void ParseProperties(JObject obj, string properties)
        {
            if (string.IsNullOrWhiteSpace(properties))
            {
                return;
            }

            var splitEventProperties = properties.Split(new[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
            var keyValueSeparatorForSplit = new[] { ':' };

            foreach (var sep in splitEventProperties)
            {
                var splitEventProperty = sep.Split(keyValueSeparatorForSplit, StringSplitOptions.RemoveEmptyEntries);
                if (splitEventProperty.Length < 2)
                {
                    // Invalid key value pair.
                    continue;
                }

                var key = splitEventProperty[0].Trim();
                var stringValue = splitEventProperty[1].Trim();
                if (key.Length == 0)
                {
                    // Empty key is not valid.
                    continue;
                }

                if (long.TryParse(stringValue, out var longValue))
                {
                    obj[key] = longValue;
                }
                else if (decimal.TryParse(stringValue, out var decimalValue))
                {
                    obj[key] = decimalValue;
                }
                else if (bool.TryParse(stringValue, out var boolValue))
                {
                    obj[key] = boolValue;
                }
                else
                {
                    obj[key] = stringValue;
                }
            }
        }
    }
}
﻿using McMaster.Extensions.CommandLineUtils;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Seq.Client.CommandLine
{
    internal static class Program
    {
        private static readonly Dictionary<string, LogLevel> LogLevels = new Dictionary<string, LogLevel>(StringComparer.OrdinalIgnoreCase)
        {
            ["verbose"] = LogLevel.Verbose,
            ["v"] = LogLevel.Verbose,
            ["debug"] = LogLevel.Debug,
            ["d"] = LogLevel.Debug,
            ["information"] = LogLevel.Information,
            ["info"] = LogLevel.Information,
            ["i"] = LogLevel.Information,
            ["warning"] = LogLevel.Warning,
            ["warn"] = LogLevel.Warning,
            ["w"] = LogLevel.Warning,
            ["error"] = LogLevel.Error,
            ["e"] = LogLevel.Error,
            ["fatal"] = LogLevel.Fatal,
            ["f"] = LogLevel.Fatal
        };

        private static bool _verbose;

        internal static int Main(string[] args)
        {
            var app = new CommandLineApplication
            {
                Name = "Command-line client for Seq",
                Description = ".NET Core command-line client for Seq log server."
            };

            app.HelpOption("-?|-h|--help");

            var verboseOption = app.Option("-v|--verbose", "Show (debug) messages produced while sending log events to Seq", CommandOptionType.NoValue);
            var logLevelOption = app.Option("-l|--log-level <logLevel>", "Log level (verbose/v, debug/d, information/info/i, warning/warn/w, error/e, fatal/f)", CommandOptionType.SingleValue);
            var sourceContextOption = app.Option("-s|--source-context <sourceContext>", "Source context, that is, where log event was generated", CommandOptionType.SingleValue);
            var messageTemplateOption = app.Option("-m|--message-template <messageTemplate>", "Log message (with optional template syntax)", CommandOptionType.SingleValue);
            var propertiesOption = app.Option("-p|--properties <properties>", "Optional event properties (syntax: \"NumberParam:21|StringParam:Hello\")", CommandOptionType.SingleValue);
            var exceptionOption = app.Option("-x|--exception <exception>", "Optional exception description (stack trace, for example)", CommandOptionType.SingleValue);
            var baseUrlOption = app.Option("-b|--base-url <baseUrl>", "Base URL. Internal use only", CommandOptionType.SingleValue);
            var apiKeyOption = app.Option("-k|--api-key <apiKey>", "Optional API KEY. If not specified, API KEY defined in launcher script will be used", CommandOptionType.MultipleValue);
            var defaultPropertiesOption = app.Option("-d|--default-properties", "Default properties (like host name) attached to all events (syntax: \"NumberParam:21|StringParam:Hello\")", CommandOptionType.SingleValue);

            app.OnExecute(async () =>
            {
                _verbose = verboseOption.HasValue();

                var logLevel = LogLevel.Verbose;
                var hasLogLevel = logLevelOption.HasValue() && LogLevels.TryGetValue(logLevelOption.Value(), out logLevel);
                if (!hasLogLevel)
                {
                    throw new ArgumentException("A valid log level must be provided", nameof(logLevel));
                }

                var sourceContext = string.Empty;
                var hasSourceContext = sourceContextOption.HasValue() && !string.IsNullOrWhiteSpace(sourceContext = sourceContextOption.Value());
                if (!hasSourceContext)
                {
                    throw new ArgumentException("A valid source context must be provided", nameof(sourceContext));
                }

                var messageTemplate = string.Empty;
                var hasMessageTemplate = messageTemplateOption.HasValue() && !string.IsNullOrWhiteSpace(messageTemplate = messageTemplateOption.Value());
                if (!hasMessageTemplate)
                {
                    throw new ArgumentException("A valid message template must be provided", nameof(messageTemplate));
                }

                var baseUrl = default(Uri);
                var hasBaseUrl = baseUrlOption.HasValue() && Uri.TryCreate(baseUrlOption.Value(), UriKind.Absolute, out baseUrl);
                if (!hasBaseUrl)
                {
                    throw new ArgumentException("A valid base URL must be provided", nameof(baseUrl));
                }

                // Get last API KEY value, so that if user specifies a different API KEY, we use that
                // instead of the default one.
                var apiKey = string.Empty;
                var hasApiKey = apiKeyOption.HasValue() && !string.IsNullOrWhiteSpace(apiKey = apiKeyOption.Values[apiKeyOption.Values.Count - 1]);
                if (!hasApiKey)
                {
                    throw new ArgumentException("A valid API KEY must be provided", nameof(apiKey));
                }

                // Optional parameters.
                var properties = propertiesOption.HasValue() ? propertiesOption.Value() : string.Empty;
                var defaultProperties = defaultPropertiesOption.HasValue() ? defaultPropertiesOption.Value() : string.Empty;
                var exception = exceptionOption.HasValue() ? exceptionOption.Value() : string.Empty;

                try
                {
                    var logEvent = new LogEvent
                    {
                        Timestamp = DateTimeOffset.UtcNow,
                        LogLevel = logLevel,
                        SourceContext = sourceContext,
                        MessageTemplate = messageTemplate,
                        Properties = properties,
                        DefaultProperties = defaultProperties,
                        Exception = exception
                    }.ToClef();

                    Information("Log event, compact format: {0}", logEvent);
                    Information("API KEY: {0}", apiKey);

                    using (var httpClient = new HttpClient())
                    using (var stringContent = new StringContent(logEvent, Encoding.UTF8, "application/json"))
                    {
                        httpClient.BaseAddress = baseUrl;
                        httpClient.DefaultRequestHeaders.Add("X-Seq-ApiKey", apiKey);
                        await httpClient.PostAsync("/api/events/raw?clef", stringContent);
                    }

                    Information("Log event successfully sent :-)");
                }
                catch (Exception ex)
                {
                    ex = ex.GetBaseException();
                    Error("An error has occurred while generating and sending log event - {0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace);
                    // This exception should not be related to invalid command-line arguments and
                    // exit code should be OK.
                }

                // All done, exit with OK code.
                return 0;
            });

            try
            {
                return app.Execute(args);
            }
            catch (Exception ex)
            {
                ex = ex.GetBaseException();
                Error("An error has occurred while parsing command-line arguments - {0}{1}{2}", ex.Message, Environment.NewLine, ex.StackTrace);

                // Show help and exit with an error.
                app.ShowHint();
                return 1;
            }
        }

        private static void Information(string msg, params object[] args)
        {
            if (!_verbose)
            {
                return;
            }
            Console.Out.WriteLine(msg, args);
        }

        private static void Error(string msg, params object[] args)
        {
            if (!_verbose)
            {
                return;
            }
            try
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Error.WriteLine(msg, args);
            }
            finally
            {
                Console.ResetColor();
            }
        }
    }
}
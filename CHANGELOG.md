# Changelog for Seq.Client.CommandLine

## v1.1.0 (2018-02-19)

* Updated dependencies.

## v1.0.0 (2017-11-25)

* Initial release.
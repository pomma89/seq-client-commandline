# Seq Client for Command-line

*Command-line utility which sends log events to a [Seq](https://getseq.net/) instance via its REST APIs.*

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA)

__NOTICE__: After this project was created, Seq team built an [official command-line client](https://github.com/datalust/seqcli). Please consider using that client instead of this project.

## How to build

Please make sure that you have .NET Core 2.0 SDK, or a more recent version, installed on your machine:

* [Prerequisites for .NET Core on Windows](https://docs.microsoft.com/en-us/dotnet/core/windows-prerequisites?tabs=netcore2x)
* [Prerequisites for .NET Core on Linux](https://docs.microsoft.com/en-us/dotnet/core/linux-prerequisites?tabs=netcore2x)

Then, clone this project and open a terminal where you cloned it. In order to obtain production builds, you should run following command:

* On Windows: `.\build.cmd`
* On Linux: `./build.sh`

Then, if .NET Core 2.0 SDK was properly setup, you should find production builds inside `artifacts` folder. Each subfolder contains a [self-contained deployment (SCD)](https://docs.microsoft.com/en-us/dotnet/core/deploying/#self-contained-deployments-scd) for a specific environment (Windows x86, Windows x64, Linux x64).

## How to install

Before proceeding, please make sure that following pieces of information are available to you:

* The path where Seq command-line utility will be deployed.
* The URL of Seq instance (host name and port number).
* A Seq API KEY which will be used as default key.
* Which name will be used to activate command-line utility (default is `seq-log`).

With [self-contained deployments (SCD)](https://docs.microsoft.com/en-us/dotnet/core/deploying/#self-contained-deployments-scd), it is not required for .NET Core to also be installed on target system. However, when deploying to a Linux environment, you should at least make sure that a few core libraries are available (like ICU, OpenSSL and cURL).

Then, choose one SCD, depending on target environment, and copy it inside the path you chose. Open the deployment and find `seq-log.cmd` on Windows, or `seq-log` on Linux, and:

1. If you decided that Seq command-line utility should be called with another name, please rename the file.
1. Open it and look for initial variables.
1. Set `SEQ_BASE_URL` to point to your Seq instance.
1. Set `SEQ_DEFAULT_API_KEY` to the API KEY you chose.
1. Alter `DEFAULT_PROPERTIES` by adding new properties shared by all events (more on this later).

As a final step, make sure that `PATH` global variable points to where you copied the SCD.

## How to use

Following commands work both on Windows and Linux and it is assumed that you did not change utility name (otherwise, replace old name with new one).

As expected, running:

```sh
seq-log --help
```

Prints out the following documentation:

```txt
Usage: Command-line client for Seq [options]

Options:
  -?|-h|--help                             Show help information
  -v|--verbose                             Show (debug) messages produced while sending log events to Seq
  -l|--log-level <logLevel>                Log level (verbose/v, debug/d, information/info/i, warning/warn/w, error/e, fatal/f)
  -s|--source-context <sourceContext>      Source context, that is, where log event was generated
  -m|--message-template <messageTemplate>  Log message (with optional template syntax)
  -p|--properties <properties>             Optional event properties (syntax: "NumberParam:21|StringParam:Hello")
  -x|--exception <exception>               Optional exception description (stack trace, for example)
  -b|--base-url <baseUrl>                  Base URL. Internal use only
  -k|--api-key <apiKey>                    Optional API KEY. If not specified, API KEY defined in launcher script will be used
  -d|--default-properties                  Default properties (like host name) attached to all events (syntax: "NumberParam:21|StringParam:Hello")
```

Then, please have a look at integration tests ([Windows](https://gitlab.com/pomma89/seq-client-commandline/blob/master/test/Seq.Client.CommandLine.IntegrationTests/seq-log-self-test.cmd) or [Linux](https://gitlab.com/pomma89/seq-client-commandline/blob/master/test/Seq.Client.CommandLine.IntegrationTests/seq-log-self-test.sh)) in order to find a lot of examples on how to use this utility.

## About this repository and its maintainer

Everything done on this repository is freely offered on the terms of the project license. You are free to do everything you want with the code and its related files, as long as you respect the license and use common sense while doing it :-)

I maintain this project during my spare time, so I can offer limited assistance and I can offer **no kind of warranty**. This project is an **unofficial client** for [Seq](https://getseq.net/) and I am in no way related to [Seq](https://getseq.net/) maintainers and developers (except that I really like their product, of course).

However, if this project helps you, then you might offer me an hot cup of coffee:

[![Donate](https://pomma89.gitlab.io/images/buy-me-a-coffee.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=ELJWKEYS9QGKA)

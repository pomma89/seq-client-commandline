@echo off

rd /q /s "artifacts" 2>nul

dotnet publish "src/Seq.Client.CommandLine" -c Release -r win-x86 -o "../../artifacts/win-x86"
dotnet publish "src/Seq.Client.CommandLine" -c Release -r win-x64 -o "../../artifacts/win-x64"
dotnet publish "src/Seq.Client.CommandLine" -c Release -r linux-x64 -o "../../artifacts/linux-x64"
